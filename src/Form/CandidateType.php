<?php

namespace App\Form;

use App\Entity\Candidate;
use App\Entity\Degree;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;

class CandidateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, [
                'label' => 'name'
            ])
            ->add('firstname',TextType::class, [
                'label' => 'Firstname'
            ])
            ->add('age',TextType::class, [
                'constraints' => [
                    new Constraints\Length ([
                        'min' => 2,
                        'max' => 2,
                        'exactMessage' => "If not empty, the age must contain {{ limit }} character",
                    ])
                    
                ],
                'label' => 'Age'
            ])
            ->add('mail', TextType::class, [
                'constraints' => [
                    new Constraints\Email([
                        'message' => 'The email "{{ value }}" is not a valid email.',
                        'checkMX' => true,
                    ]),
                    new Constraints\Length([
                        'min' => 5,
                        'max' => 50,
                        'exactMessage' => 'The mail must composed between 5 to 20 character'
                    ]),
                        new Constraints\Regex([
                        "pattern" => '/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/',
                        "message" => 'The Username of the mail must contain /^[A-Za-z0-9]+(?:[_]{0,2}[A-Za-z0-9]+)*$/'
                    ])
                        
                ],
                'label' => 'Email *'
            ])
            ->add('phone', TextType::class, [
                'constraints' => [
                    new Constraints\Regex([
                        "pattern" => "/[0-9]{10}/",
                        'message' => "The phone number is not valid"
                    ])
                    
                ],
                'label' => 'Phone'

            ])
            ->add('numberIc', TextType::class, [
                'constraints' => [
                    new Constraints\Length ([
                        'min' => 13,
                        'max' => 20,
                        'exactMessage' => "If not empty, the identity number card must contain {{ limit }} character",
                    ])
                    
                ],
                'label' => 'Identity Number Card'
            ])
            ->add('street', TextType::class,[
                'label' => 'Street'
            ])
            ->add('zip_code',TextType::class,[
                'constraints' => [
                    new Constraints\Length([
                    'min' => 5,
                    'max' => 5,
                    'exactMessage' => 'The zip code must composed of 5 figure'
                ]),
                    new Constraints\Regex([
                    "pattern" => '/[0-9]{5}/',
                    "message" => 'The zip code must composed of 5 figure from 0 to 9'
                ])
            ],
            ])
            ->add('city',TextType::class, [
                'label' => 'City'
            ])
            ->add('country',TextType::class, [ 
                'label' => 'Country' 
            ])
            ->add('degree')

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Candidate::class,
        ]);
    }
}
