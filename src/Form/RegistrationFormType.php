<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('username',TextType::class, [
            'label' => 'Username *' 
        ])
        ->add('plainPassword', PasswordType::class, [
            'mapped' => false,
            'constraints' => [
                new Constraints\Length([
                    'min' => 6,
                    'max' => 20,
                    'minMessage' => "The password must contain at least {{ limit }} character",
                ]),
                new Constraints\Regex([
                    "pattern" => '/^(?=.*[A-z])(?=.*[0-9])(?=.*[$@])(\S{6,20})$/',
                    "message" => 'The Username of the mail must contain /^[A-Za-z0-9]+(?:[_]{0,2}[A-Za-z0-9]+)*$/'
                ]),
                new Constraints\NotBlank([
                    'message' => 'Please enter the password :',
                ])
                
            ],
            'label' => 'Password *'
        ])
        ->add('lastname',TextType::class, [
            'label' => 'Lastname *'
        ])
        ->add('firstname',TextType::class, [
            'label' => 'FirstName *'
        ])
        ->add('gender', ChoiceType::class, [
            'choices' => [
                'Mr' => 'Man',
                'Mme' => 'Woman',
                'Other' => 'Other',
            ],
        ])
        ->add('age',NumberType::class, [
            'constraints' => [
                new Constraints\Length ([
                    'min' => 2,
                    'max' => 2,
                    'exactMessage' => "If not empty, the age must contain {{ limit }} character",
                ])
                
            ],
            'label' => 'Age'
        ])
        ->add('phone', TextType::class, [
            'constraints' => [
                new Constraints\Regex([
                    'pattern' => "/[0-9]{10}/",
                    'message' => "The phone number is not valid",
                ])
            ],
            'label' => 'Phone *',
            ])
        ->add('mail', TextType::class, [
            'constraints' => [
                new Constraints\Email([
                    'message' => 'The email "{{ value }}" is not a valid email.',
                    'checkMX' => true,
                ]),
                new Constraints\Length([
                    'min' => 5,
                    'max' => 50,
                    'exactMessage' => 'The mail must composed between 5 to 20 character'
                ]),
                    new Constraints\Regex([
                    "pattern" => '/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/',
                    "message" => 'The Username of the mail must contain /^[A-Za-z0-9]+(?:[_]{0,2}[A-Za-z0-9]+)*$/'
                ])
                    
            ],
            'label' => 'Email *'
        ])
        ->add('street', TextType::class,[
            'label' => 'Street *',
            'mapped' => false
        ])
        ->add('zip_code',TextType::class,[
            'constraints' => [
                new Constraints\Length([
                'min' => 5,
                'max' => 5,
                'exactMessage' => 'The zip code must composed of 5 figure'
            ]),
                new Constraints\Regex([
                "pattern" => '/[0-9]{5}/',
                "message" => 'The zip code must composed of 5 figure from 0 to 9'
            ])
        ],
            'mapped' => false,
            'label' => 'Zip Code *'
        ])
        ->add('city',TextType::class, [
            'label' => 'City *',
            'mapped' => false
        ])
        ->add('country',TextType::class, [
            'label' => 'Country *',
            'mapped' => false
        ])
        
        ->add('agreeTerms', CheckboxType::class, [
            'mapped' => false,
            'constraints' => [
                new IsTrue([
                    'message' => 'You should agree to our terms.',
                ]),
            ],
        ])
        ->add('register',SubmitType::class, [
            'attr' => [
                'class' => 'btn-dark'
            ]
        ])
    ;
}

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,

        ]);
    }
}
