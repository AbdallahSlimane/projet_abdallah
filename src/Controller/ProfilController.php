<?php

namespace App\Controller;

use App\Form\ProfilType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class ProfilController extends AbstractController
{
 /**
    * @Route("/profil", name="profil", methods={"GET"})
    * 
    */
    public function profil()
    {

        return $this->render('profil/profil.html.twig', []);
    }

        /**
    * @Route("/profil/modifier", name="profil_modifier", methods={"GET","POST"})
    */
    public function profil_modifier(Request $request, UserRepository $userRepository)
    {
        $user = $this->getUser();
        $form = $this->createForm(ProfilType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($mdp = $form->get('passwword')->getData()){
                $user->setPassWord(password_hash($mdp,PASSWORD_DEFAULT));
            }else{
                $user->setPassword($userRepository->find($user->getId()->getPassword()));
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
